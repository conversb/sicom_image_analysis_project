import os
import cv2

def folderExists(path):
	CHECK_FOLDER = os.path.isdir(path)

	# If folder doesn't exist, it creates it.
	if not CHECK_FOLDER:
		os.makedirs(path)
		print("[DATA] You created a new folder : " +  str(path))

import numpy as np

def quad_bayer_to_bayer(quad_bayer_pattern):
    # We test that thee quad bayer size fit with a multiple of 2 for width and height). If not, we pad it.
	if quad_bayer_pattern.shape[0] % 2 != 0 or quad_bayer_pattern.shape[1] % 2 != 0:
		print("[INFO] The quad bayer pattern size is not valid. We need to pad it.")

		pad_schema = []

		if quad_bayer_pattern.shape[0] % 2 != 0:
				pad_schema.append([0, 1])

		if quad_bayer_pattern.shape[1] % 2 != 0:
				pad_schema.append([0, 1])
		else:
				pad_schema.append([0, 0])

		quad_bayer_pattern = np.pad(quad_bayer_pattern, pad_schema, mode="reflect")[:, 2:]

	# we create a new bayer pattern with the good size
	bayer_pattern = np.zeros((quad_bayer_pattern.shape[0] // 2, quad_bayer_pattern.shape[1] // 2))

	# We combine adjacent pixels to create the Bayer pattern
	for i in range(0, quad_bayer_pattern.shape[0], 2):
		for j in range(0, quad_bayer_pattern.shape[1], 2):
			bayer_pattern[i // 2, j // 2] = (
			quad_bayer_pattern[i, j] +
			quad_bayer_pattern[i, j + 1] +
			quad_bayer_pattern[i + 1, j] +
			quad_bayer_pattern[i + 1, j + 1]
		) / 4

	# We resize bayer iamge to the original image size

	#return cv2.resize(bayer_pattern, quad_bayer_pattern.shape, interpolation=cv2.INTER_CUBIC)
	return bayer_pattern
