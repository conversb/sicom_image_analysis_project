"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
import cv2
from src.forward_model import CFA
from src.methods.brice_convers.menon import demosaicing_CFA_Bayer_Menon2007
import src.methods.brice_convers.configuration as configuration
from src.methods.brice_convers.utilities import quad_bayer_to_bayer


def run_reconstruction(y: np.ndarray, cfa: str) -> np.ndarray:
    """Performs demosaicking on y.

    Args:
        y (np.ndarray): Mosaicked image to be reconstructed.
        cfa (str): Name of the CFA. Can be bayer or quad_bayer.

    Returns:
        np.ndarray: Demosaicked image.
    """

    input_shape = (y.shape[0], y.shape[1], 3)
    op = CFA("bayer", input_shape)

    if cfa == "quad_bayer":
        y = quad_bayer_to_bayer(y)
        op = CFA("bayer", y.shape)

    reconstructed_image = demosaicing_CFA_Bayer_Menon2007(y, op.mask, configuration.PIXEL_PATTERN, configuration.REFINING_STEP)

    if cfa == "quad_bayer":
        return cv2.resize(reconstructed_image, input_shape[:2], interpolation=cv2.INTER_CUBIC)
    else:
        return reconstructed_image

####
####
####
####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
