import src.methods.brice_convers.dataHandler as DataHandler
import src.methods.brice_convers.dataEvaluation as DataEvaluation

WORKING_DIRECOTRY_PATH = "SICOM_Image_Analysis/sicom_image_analysis_project/"

DataHandler = DataHandler.DataHandler(WORKING_DIRECOTRY_PATH)
DataEvaluation = DataEvaluation.DataEvaluation(DataHandler)

def main(DataHandler):

    IMAGE_PATH = WORKING_DIRECOTRY_PATH + "images/"

    CFA_NAME = "quad_bayer"

    DataHandler.list_images(IMAGE_PATH)

    DataHandler.compute_CFA_images(CFA_NAME)

    DataHandler.compute_reconstruction_images("menon", {"pattern": "RGB", "refining_step": True})

    DataHandler.plot_reconstructed_image(2, "menon")

    DataEvaluation.print_metrics(2)

    print("[INFO] End")

if __name__ == "__main__":
    main(DataHandler)
